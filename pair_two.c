#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

#define BLUEZ_SERVICE "org.bluez"

#define AGENT_PATH "/test/agent"
#define AGENT_INTERFACE "org.bluez.Agent1"

static int master_is_acceptor;
static int pairing;
static enum { PAIR_ONGOING, PAIR_SUCCESS, PAIR_FAIL } paired = PAIR_ONGOING;

void usage(const char *name)
{
    printf("Usage: %s <master adapter> <slave address|slave adapter>\n", name);
    printf("       %s hci0 hci1                  pair the adapters\n", name);
    printf("       %s hci0 bb:dd:aa:dd:dd:22     initiate pairing to device from adapter\n", name);
    printf("       %s hci0 bb:dd:aa:dd:dd:22 -l  accept pairing from device on adapter\n", name);
}

int get_boolean_property(sd_bus * bus, const char *service,
                         const char *path, const char *interface,
                         const char *member)
{
    int err;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *message = NULL;
    int state;

    err = sd_bus_get_property(bus, service, path, interface, member,
                              &error, &message, "b");
    if (err < 0) {
        fprintf(stderr, "Failed to retrieve property\n");
        return err;
    }

    err = sd_bus_message_read(message, "b", &state);
    if (err < 0) {
        fprintf(stderr, "Failed to parse response message\n");
        return err;
    }

    return ! !state;
}

int set_boolean_property(sd_bus * bus, const char *service,
                         const char *path, const char *interface,
                         const char *member, int state)
{
    int err;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *message = NULL;

    err = sd_bus_set_property(bus, service, path, interface, member,
                              &error, "b", state);
    if (err < 0) {
        fprintf(stderr, "Failed to set property\n");
        return err;
    }

    return err;
}

int get_str_property(sd_bus * bus, const char *service,
                     const char *path, const char *interface,
                     const char *member, char *buf, int size)
{
    int err;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *message = NULL;
    int state;
    char *value;

    err = sd_bus_get_property(bus, service, path, interface, member,
                              &error, &message, "s");
    if (err < 0) {
        fprintf(stderr, "Failed to retrieve property\n");
        return err;
    }

    err = sd_bus_message_read(message, "s", &value);
    if (err < 0) {
        fprintf(stderr, "Failed to parse response message\n");
        return err;
    }

    snprintf(buf, size, "%s", value);

    return 0;
}

int basic_setup(sd_bus * bus, const char *path)
{
    int err;

    err = get_boolean_property(bus, BLUEZ_SERVICE, path,
                               "org.bluez.Adapter1", "Pairable");
    if (err < 0) {
        fprintf(stderr, "Failed to retrieve property\n");
        return err;
    }

    printf("Pairable: %s\n", (err) ? "True" : "False");

    if (!err) {
        err = set_boolean_property(bus, BLUEZ_SERVICE, path,
                                   "org.bluez.Adapter1", "Pairable", true);
        if (err < 0) {
            fprintf(stderr, "Failed to set property\n");
            return err;
        }
    }

    err = get_boolean_property(bus, BLUEZ_SERVICE, path,
                               "org.bluez.Adapter1", "Discoverable");
    if (err < 0) {
        fprintf(stderr, "Failed to retrieve property\n");
        return err;
    }

    printf("Discoverable: %s\n", (err) ? "True" : "False");

    if (!err) {
        err = set_boolean_property(bus, BLUEZ_SERVICE, path,
                                   "org.bluez.Adapter1", "Discoverable",
                                   true);
        if (err < 0) {
            fprintf(stderr, "Failed to set property\n");
            return err;
        }
    }

    return 0;
}

static int request_pincode(sd_bus_message * m, void *userdata,
                                sd_bus_error * ret_error)
{
    const char *device;
    const char *pincode = "0000";
    int err;

    /* Read the parameters */
    err = sd_bus_message_read(m, "o", &device);
    if (err < 0) {
        fprintf(stderr, "Failed to parse parameters\n");
        return err;
    }

    printf("RequestPincode (%s) return %s\n", device, pincode);

    /* Accept any passkey */
    return sd_bus_reply_method_return(m, "s", pincode, NULL);
}

static int request_passkey(sd_bus_message * m, void *userdata,
                                sd_bus_error * ret_error)
{
    const char *device;
    unsigned int passkey = 0;
    int err;

    /* Read the parameters */
    err = sd_bus_message_read(m, "o", &device);
    if (err < 0) {
        fprintf(stderr, "Failed to parse parameters\n");
        return err;
    }

    printf("RequestPasskey (%s) return %d\n", device, passkey);

    /* Accept any passkey */
    return sd_bus_reply_method_return(m, "u", passkey, NULL);
}

static int request_confirmation(sd_bus_message * m, void *userdata,
                                sd_bus_error * ret_error)
{
    const char *device;
    unsigned int passkey;
    int err;

    /* Read the parameters */
    err = sd_bus_message_read(m, "ou", &device, &passkey);
    if (err < 0) {
        fprintf(stderr, "Failed to parse parameters\n");
        return err;
    }

    printf("RequestConfirmation (%s, %d)\n", device, passkey);

    /* Accept any passkey */
    return sd_bus_reply_method_return(m, NULL);
}

static const sd_bus_vtable agent_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_METHOD("RequestConfirmation", "ou", NULL, request_confirmation,
                  SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_METHOD("RequestPasskey", "o", NULL, request_passkey,
                  SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_METHOD("RequestPinCode", "o", NULL, request_pincode,
                  SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_VTABLE_END
};

int register_agent(sd_bus * bus)
{
    int err;
    sd_bus_slot *slot = NULL;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *message = NULL;

    err = sd_bus_add_object_vtable(bus, &slot, AGENT_PATH, AGENT_INTERFACE,
                                   agent_vtable, NULL);
    if (err < 0) {
        fprintf(stderr, "Failed to create object vtable\n");
        return err;
    }

    err = sd_bus_call_method(bus, BLUEZ_SERVICE, "/org/bluez",
                             "org.bluez.AgentManager1", "RegisterAgent",
                             &error, &message, "os", AGENT_PATH, "", NULL);
    if (err < 0) {
        fprintf(stderr, "Failed to create register agent\n");
        return err;
    }

    err = sd_bus_call_method(bus, BLUEZ_SERVICE, "/org/bluez",
                             "org.bluez.AgentManager1", "RequestDefaultAgent",
                             &error, &message, "o", AGENT_PATH, NULL);
    if (err < 0) {
        fprintf(stderr, "Failed to request default agent\n");
        return err;
    }

    return err;
}

int set_device_trusted(sd_bus_message * m, const char* slave_device)
{
    sd_bus *bus = sd_bus_message_get_bus(m);
    int err = 0;

    err = set_boolean_property(bus, BLUEZ_SERVICE, slave_device,
                               "org.bluez.Device1", "Trusted", true);

    if (err < 0) {
        fprintf(stderr, "Failed to pair %s: %s\n", slave_device,
                strerror(-err));
        paired = PAIR_FAIL;
    } else {
        printf("Successfully paired to %s\n", slave_device);
        paired = PAIR_SUCCESS;
    }
    return 0;
}

int device_pair_handler(sd_bus_message * m, void *userdata,
                         sd_bus_error * ret_error)
{
    char *slave_device = userdata;
    int err = 0;

    if (sd_bus_error_is_set(ret_error)) {
        err = -sd_bus_error_get_errno(ret_error);
    }

    if (err < 0) {
        fprintf(stderr, "Failed to pair %s: %s\n", slave_device,
                strerror(-err));
        paired = PAIR_FAIL;
    } else {
        set_device_trusted(m, slave_device);
    }

    return 0;
}

int interface_added_handler(sd_bus_message * m, void *userdata,
                            sd_bus_error * ret_error)
{
    char *slave_device = userdata;
    const char *device_path;
    int err;

    /* Read the parameters */
    err = sd_bus_message_read(m, "o", &device_path);
    if (err < 0) {
        fprintf(stderr, "Failed to parse PropertyChanged\n");
        return err;
    }

    printf("Device added: %s\n", slave_device);

    if (strcmp(slave_device, device_path) != 0)
        return 0;

    /* Don't pair if master is acceptor or a pairing is in progress*/
    if (master_is_acceptor || pairing)
        return 0;

    err = sd_bus_call_method_async(sd_bus_message_get_bus(m), NULL, BLUEZ_SERVICE,
                           slave_device, "org.bluez.Device1", "Pair",
                           device_pair_handler, slave_device,
                           NULL);
    if (err < 0) {
        fprintf(stderr, "Failed to trigger Pairing\n");
        return err;
    }

    printf("Pairing requested: %s\n", slave_device);

    pairing = 1;
    return 0;
}

int paired_property_changed_handler(sd_bus_message * m, void *userdata,
                                    sd_bus_error * ret_error)
{
    const char *interface_name;
    const char *prop_name;
    const char *contents;
    char *slave_device = userdata;
    int paired = 0;
    int err;

    /* Read the parameters */
    err = sd_bus_message_read(m, "s", &interface_name);
    if (err < 0) {
        fprintf(stderr, "Failed to parse PropertiesChanged\n");
        return err;
    }

    printf("Property changed: %s\n", interface_name);

    err = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");

    while ((err = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv")) > 0) {
        char type;

        err = sd_bus_message_read(m, "s", &prop_name);
        if (err < 0)
            return err;

        err = sd_bus_message_peek_type(m, &type, &contents);
        if (err < 0)
            return err;

        /* Need to enter the variant, regarless we want it or not */
        err = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, contents);
        if (err < 0)
            return err;

        if (strcmp(prop_name, "Paired") == 0) {
            err = sd_bus_message_read(m, "b", &paired);
            if (err < 0)
                return err;

            printf("    Property: %s=%d\n", prop_name, paired);
        } else {
            err = sd_bus_message_skip(m, contents);
            if (err < 0)
                return err;

            printf("    Property: %s\n", prop_name);
        }
        /* variant */
        err = sd_bus_message_exit_container(m);
        if (err < 0)
            return err;

        /* dict entry */
        err = sd_bus_message_exit_container(m);
        if (err < 0)
            return err;
    }

    /* array */
    err = sd_bus_message_exit_container(m);
    if (err < 0)
        return err;

    /* If the paired property changed to TRUE: let's quit */
    if (paired)
        set_device_trusted(m, slave_device);

    return 0;
}

int monitor_devices(sd_bus * bus, char *master, char *slave_device)
{
    int err;
    char match [512];

    snprintf (match, sizeof (match), "type='signal',"
              "interface='org.freedesktop.DBus.ObjectManager',"
              "member='InterfacesAdded',"
              "path=/,");
    printf("Matching %s\n", match);

    err = sd_bus_add_match(bus, NULL, match,
                           interface_added_handler, slave_device);
    if (err < 0) {
        fprintf(stderr, "Unable to monitor interfaces\n");
        return err;
    }

    snprintf (match, sizeof (match), "type='signal',"
              "interface='org.freedesktop.DBus.Properties',"
              "member='PropertiesChanged',"
              "arg0='org.bluez.Device1'");
    printf("Matching %s\n", match);

    err = sd_bus_add_match(bus, NULL, match,
                           paired_property_changed_handler, slave_device);
    if (err < 0) {
        fprintf(stderr, "Unable to monitor pairing changes\n");
        return err;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    char *master;
    char *slave;
    int slave_is_adapter = 0;
    char master_adapter[256];
    char master_address[256];
    char master_device[256];
    char slave_adapter[256];
    char slave_address[256];
    char slave_device[256];
    sd_bus *bus = NULL;
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *message = NULL;
    int i;
    int err;

    /* We need 2 arguments to be supplied */
    if (argc < 3) {
        usage(argv[0]);
        return 1;
    }

    master = argv[1];
    slave = argv[2];
    /* If the slave parameters starts with hci, it's an adapter,
     * else, it's a device bdaddr */
    if (strncmp(slave, "hciX", 3) == 0)
        slave_is_adapter = 1;

    /* If a third argument exists
     * start an acceptor role */
    if (argc >= 4 && strcmp(argv[3], "-l") == 0)
        master_is_acceptor = 1;

    printf("Master: %s\n", master);
    printf("Slave: %s (%s)\n", slave, slave_is_adapter?"adapter":"bdaddr");

    err = sd_bus_open_system(&bus);
    if (err < 0) {
        fprintf(stderr, "Unable to open system bus\n");
        return 1;
    }

    /* Setup object names */
    snprintf(master_adapter, sizeof(master_adapter), "/org/bluez/%s",
             master);
    snprintf(slave_adapter, sizeof(slave_adapter), "/org/bluez/%s", slave);

    /* Get device addresses */
    err = get_str_property(bus, BLUEZ_SERVICE, master_adapter,
                           "org.bluez.Adapter1", "Address",
                           master_address, sizeof(master_address));
    if (err < 0) {
        fprintf(stderr, "Failed to get master address\n");
        goto err;
    }
    printf("Master address: %s\n", master_address);

    if (slave_is_adapter) {
        err = get_str_property(bus, BLUEZ_SERVICE, slave_adapter,
                               "org.bluez.Adapter1", "Address",
                               slave_address, sizeof(slave_address));
        if (err < 0) {
            fprintf(stderr, "Failed to get slave address\n");
            goto err;
        }
    } else {
        snprintf(slave_address, sizeof(slave_address), "%s", slave);
    }
    printf("Slave address: %s\n", slave_address);

    /* Remove slave pairing on master adapter */
    snprintf(slave_device, sizeof(slave_device), "/org/bluez/%s/dev_%s",
             master, slave_address);
    for (i = 0; slave_device[i]; i++)
        if (slave_device[i] == ':')
            slave_device[i] = '_';
    err = sd_bus_call_method(bus, BLUEZ_SERVICE, master_adapter,
                             "org.bluez.Adapter1", "RemoveDevice",
                             &error, &message, "o", slave_device, NULL);
    if (err < 0) {
        fprintf(stderr, "Failed to remove %s: %s\n", slave_device,
                strerror(-err));
        if (!sd_bus_error_has_name(&error, "org.bluez.Error.DoesNotExist"))
            goto err;
        sd_bus_message_unref(message);
        message = NULL;
        error = SD_BUS_ERROR_NULL;
    }

    /* Remove master pairing on slave adapter */
    if (slave_is_adapter) {
        snprintf(master_device, sizeof(master_device),
                 "/org/bluez/%s/dev_%s", slave, master_address);
        for (i = 0; master_device[i]; i++)
            if (master_device[i] == ':')
                master_device[i] = '_';
        err = sd_bus_call_method(bus, BLUEZ_SERVICE, slave_adapter,
                                 "org.bluez.Adapter1", "RemoveDevice",
                                 &error, &message, "o", master_device,
                                 NULL);
        if (err < 0) {
            fprintf(stderr, "Failed to remove %s: %s\n", master_device,
                    strerror(-err));
            if (!sd_bus_error_has_name
                (&error, "org.bluez.Error.DoesNotExist"))
                goto err;
            sd_bus_message_unref(message);
            message = NULL;
            error = SD_BUS_ERROR_NULL;
        }

        /* Make slave pairable and discoverable */
        err = basic_setup(bus, slave_adapter);
        if (err < 0) {
            fprintf(stderr, "Failed to configure slave adapter\n");
            goto err;
        }
    }

    /* Register Agent */
    err = register_agent(bus);
    if (err < 0) {
        fprintf(stderr, "Failed to register agent\n");
        goto err;
    }

    /* Monitor the adapter until devices are added */
    err = monitor_devices(bus, master_adapter, slave_device);
    if (err < 0) {
        fprintf(stderr, "Failed to monitor errors\n");
        goto err;
    }

    /* Perform scan from master */
    if (!master_is_acceptor) {
        err = sd_bus_call_method(bus, BLUEZ_SERVICE, master_adapter,
                                 "org.bluez.Adapter1", "StartDiscovery",
                                 &error, &message, NULL);
        if (err < 0) {
            fprintf(stderr, "Failed to start discovery: %s\n", strerror(-err));
            goto err;
        }
        printf("Scanning\n");
    } else {
        /* Make slave pairable and discoverable */
        err = basic_setup(bus, master_adapter);
        if (err < 0) {
            fprintf(stderr, "Failed to configure adapter: %s\n", strerror(-err));
            goto err;
        }

        printf("Waiting for pairing\n");
    }

    /* Main loop, waits for slave to appear in scan result */
    while (paired == PAIR_ONGOING) {
        err = sd_bus_process(bus, NULL);
        if (err < 0) {
            printf("[dbus] Failed to process bus: %s", strerror(-err));
            break;
        }

        if (paired != PAIR_ONGOING)
            break;

        err = sd_bus_wait(bus, -1);
        if (err < 0) {
            printf("[dbus] Failed to wait bus: %s", strerror(-err));
            break;
        }
    }

    /* Perform scan */
    if (!master_is_acceptor) {
        err = sd_bus_call_method(bus, BLUEZ_SERVICE, master_adapter,
                                 "org.bluez.Adapter1", "StopDiscovery",
                                 &error, &message, NULL);
        if (err < 0) {
            fprintf(stderr, "Failed to stop discovery: ignoring\n");
        }
    }

  err:
    sd_bus_error_free(&error);
    sd_bus_message_unref(message);
    sd_bus_unref(bus);

    printf("Done\n");
    return (paired == PAIR_SUCCESS) ? 0 : 1;
}
